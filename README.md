Some convenience functions for using arcpy.mapping module. Obviously you need
arcpy installed.

Install with e.g.

    pip install -e
    git+https://bitbucket.org/blindjesse/arcmapping.git/#egg=arcmapping


