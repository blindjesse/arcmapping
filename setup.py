from setuptools import setup 

setup(name='arcmapping',
      version='0.1.4',
      description='Convenience functions for arcpy.mapping',
      url='http://bitbucket.org/blindjesse/arcmapping',
      author='Jesse Anderson',
      author_email='jesse@csp-inc.org',
      license='CSP',
      packages=['arcmapping'],
      install_requires=[],
      zip_safe=True)

