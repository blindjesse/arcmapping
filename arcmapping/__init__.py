import arcpy
import sys
import os

def set_relative_path(mxd):
    if not mxd.relativePaths:
        mxd.relativePaths = True
        mxd.save()

def data_frame(mxd, name):
    dfs = arcpy.mapping.ListDataFrames(mxd)
    available_names = [d.name for d in dfs]
    if name not in available_names:
        sys.exit("ERROR: Data frame \"" + name + "\" not found in \"" + 
                mxd.filePath + "\"")
    return [d for d in dfs if d.name == name][0]

def layer(mxd, df, name):
    layers = arcpy.mapping.ListLayers(mxd, data_frame=df)
    available_names = [d.name for d in layers]
    if name not in available_names:
        sys.exit("ERROR: Layer \"" + name + "\" not found in data frame \"" + 
                df.name + "\"") 
    return [l for l in layers if l.name == name][0]

def text_element(mxd, name):
    elements = arcpy.mapping.ListLayoutElements(mxd, "TEXT_ELEMENT")
    available_names = [e.name for e in elements]
    if name not in available_names:
        sys.exit("ERROR: Text element \"" + name + "\" not found in \"" +
                 mxd.filePath + "\"")
    return [e for e in elements if e.name == name][0]

def change_text(mxd, text_element_name, text):
    element = text_element(mxd, text_element_name)
    # Works around a bug where text elements 
    # are shifted when changing text
    elem_x = element.elementPositionX
    elem_y = element.elementPositionY
    element.text = text
    element.elementPositionX = elem_x
    element.elementPositionY = elem_y

def turn_on(layer_name, data_frame_name, mxd):
    df = data_frame(mxd, data_frame_name)
    l = layer(mxd, df, layer_name)
    l.visible = True

def turn_off(layer_name, data_frame_name, mxd):
    df = data_frame(mxd, data_frame_name)
    l = layer(mxd, df, layer_name)
    l.visible = False

def set_frame_extent(frame, layer, zoom_out=1500, minimum_scale=0):
    poly_extent = layer.getSelectedExtent()
    poly_extent.XMin -= zoom_out
    poly_extent.YMin -= zoom_out
    poly_extent.XMax += zoom_out
    poly_extent.YMax += zoom_out
    frame.extent = poly_extent
    scale = frame.scale
    if (scale < minimum_scale):
        frame.scale = minimum_scale

def center_text_horizontally(text_element,page_width=11.0):
    page_center = page_width / 2.0
    element_center = text_element.elementWidth / 2.0
    text_element.elementPositionX = page_center - element_center

